<?php

/**
 * @file
 * Primary module hooks for ImageLightBox module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme().
 */
function imagelightbox_theme() {
  return [
    'imagelightbox_formatter' => [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'link_attributes' => NULL,
        'url' => NULL,
        'image_style' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_help().
 */
function imagelightbox_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'help.page.imagelightbox') {
    $output = '';
    $output .= '<h3>' . (string) new TranslatableMarkup('About') . '</h3>';
    $output .= '<p>' . (string) new TranslatableMarkup('The ImageLightbox module offers a straightforward solution for showcasing images within a lightbox interface.') . '</p>';
    $output .= '<p>' . (string) new TranslatableMarkup('You can access additional information about this module by visiting the links provided below.') . '</p>';
    $output .= '<p>' . (string) new TranslatableMarkup('<a href=":project_link">Project page</a>', [
      ':project_link' => 'https://www.drupal.org/project/imagelightbox',
    ]) . '</p>';
    $output .= '<p>' . (string) new TranslatableMarkup('<a href="@GIT"> git.drupalcode.org</a>', [
      '@GIT' => 'https://git.drupalcode.org/project/imagelightbox',
    ]) . '</p>';
    return $output;
  }
}

/**
 * Prepares variables for imagelightbox formatter templates.
 *
 * Default template: imagelightbox-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 */
function template_preprocess_imagelightbox_formatter(&$variables) {
  \Drupal::moduleHandler()->loadInclude('image', 'inc', 'image.field');
  template_preprocess_image_formatter($variables);
  $variables['link_attributes'] = new Attribute($variables['link_attributes']);
  $variables['#attached']['library'][] = 'imagelightbox/formatter';
}
