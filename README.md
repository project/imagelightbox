ImageLightBox Drupal module

Summary
=========================
This module provides integration with ImageLightBox library.


Installation
=========================
 * Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8
 * Define image style "imagelightbox" and "imagelightbox small"

Basic Configuration
=========================
 * Go to "Content Type"
 * Add a new field
   Currently supported field types are:
   - Image
   - Media/Image
 * Go to "Manage Display", select the "ImageLightBox" field formatter and
configure the image styles to use.

Custom Configuration
=========================
You can override the configuration that comes with the module by copying
the libraries/imagelightbox.config.js to your custom theme. If you put it
in the theme's js folder then you can use the following in your custom
theme's info file.


libraries-override:
  imagelightbox/formatter:
    js:
      libraries/imagelightbox.config.js: js/imagelightbox.config.js
  imagelightbox/imagelightbox:
    js:
      libraries/imagelightbox.config.js: js/imagelightbox.config.js


More information on overriding files can be found here:
https://www.drupal.org/docs/8/theming/adding-stylesheets-css-and-javascript-js-to-a-drupal-8-theme#override-extend

More information about the ImageLightBox configuration file options can
be found here: https://osvaldas.info/image-lightbox-responsive-touch-friendly

Notes
=========================
 * You can use `drush imagelightbox-download` command for easy installation
 of the library.
 * To make it work with Views you should either set "Use field template"
   checkbox or manually add "imagelightbox" class in View field style settings.
 * Download ImageLightBox library from
 https://github.com/osvaldasvalutis/imageLightbox.js.
 * Unzip the library and place files from dist directory to
   libraries/imagelightbox directory.
