<?php

namespace Drupal\imagelightbox;

/**
 * Provide dependency injection for fileUrlGenerator service.
 */
trait FileUrlGeneratorTrait {
  /**
   * The fileUrlGeneratorService.
   *
   * @var Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Set fileUrlGenerator service.
   */
  public function setFileUrlGenerator() {
    if (!$this->fileUrlGenerator) {
      $this->fileUrlGenerator = \Drupal::service('file_url_generator');
    }
    return $this->fileUrlGenerator;
  }

}
