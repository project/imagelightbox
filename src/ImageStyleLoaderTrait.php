<?php

namespace Drupal\imagelightbox;

/**
 * Provide dynamic image style loading.
 */
trait ImageStyleLoaderTrait {

  /**
   * Return image style.
   */
  protected function loadImageStyle($style_name) {
    // Load the image style dynamically.
    return \Drupal::entityTypeManager()->getStorage('image_style')->load($style_name);
  }

}
