<?php

namespace Drupal\imagelightbox;

/**
 * Provide dynamic file loading.
 */
trait FileLoaderTrait {

  /**
   * Return file.
   */
  protected function loadFile($fid) {
    // Load the file dynamically.
    return \Drupal::entityTypeManager()->getStorage('file')->load($fid);
  }

}
